FROM    os2use/ubuntu:14.04

# Bundle app source
COPY . /beacon

# Switch directory
WORKDIR /beacon

# Install app dependencies
RUN ./bootstrap.sh

VOLUME /beacon/var

EXPOSE  53
EXPOSE  3124
EXPOSE  3142

CMD ["/beacon/entrypoint.sh"]
