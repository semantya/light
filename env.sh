#!/bin/bash

case "$SHL_OS" in
  ubuntu|debian|raspbian)
    export APT_PKGs=$APT_PKGs" wvdial hostapd tor" #i2p
    #export APT_PKGs=$APT_PKGs" quagga ufw"
    export APT_PKGs=$APT_PKGs" isc-dhcp-server"
    export APT_PKGs=$APT_PKGs" ipsec openvpn xl2tp"

    export APT_PKGs=$APT_PKGs" apt-cacher-ng"
    ;;
esac

#*****************************************************************************************************

# eg: ufw_allow_network 192.168.1 24 port 22 proto tcp

ufw_allow_network () {
    ufw allow in from $1.0/$2 to $1.1 $3
}
