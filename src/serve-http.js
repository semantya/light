var http = require('http'),
    httpProxy = require('http-proxy');

//
// Create a proxy server with custom application logic
//
var proxy = httpProxy.createProxyServer({});

//
// Create your custom server and just call `proxy.web()` to proxy
// a web request to the target passed in the options
// also you can use `proxy.ws()` to proxy a websockets request
//
var Zones = {
  APT: {
    test: function (req, res) {
      if (req.host=='apt-mirror') {
       return true;
      }

      return false;
    },
    run: function (req, res) {
      proxy.web(req, res, { target: 'http://127.0.0.1:3142' });
    },
  },
  Matrix: {
    run: function () {
      proxy.web(req, res, { target: 'http://127.0.0.1:5060' });
    },
  },
};

// You can define here your custom logic to handle the request
// and then proxy the request.
var server = http.createServer(function(req, res) {
  var target=null, lst = [
    Zones.APT,
  ];

  for (var i=0 ; i<lst.length && target==null ; i++) {
    if (lst[i].test(req, res)) {
      target = lst[i];
    }
  }

  if (target==null) {
    target = Zones.Matrix;
  }
    
  target.run(req, res);
});

console.log("listening on port 3124")
server.listen(3124);
