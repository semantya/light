var dns = require('native-dns'),
    sys=require('sys'), util = require('util'), url=require('url'), S=require('string'), YAML = require('yamljs');

/**************************************************************************************************************/

exports.Context = function (req, resp) {
    this.request  = req;
    this.response = resp;
    this.params   = {};
}

exports.Context.prototype.process = function (Light) {
    // console.log(request);
    var request  = this.request;
    var response = this.response;
    
    var cnt = {
        request:  request,
        response: response,
        fqdn:     request.question[0].name,
        route:    null,
    };
    
    for (var i=0 ; (i<exports.routes.length && cnt.route==null) ; i++) {
        cnt = exports.routes[i].detect(cnt, exports.routes[i]);
    }
    
    for (var i=0 ; (i<Light.meta['aspects'].length && cnt.route==null) ; i++) {
        var aspect = Light.meta['aspects'][i];
        
        for (var j=0 ; (j<Light.objs[aspect].length && cnt.route==null) ; j++) {
            cnt = Light.objs[aspect][j].detect(cnt, Light.objs[aspect][j]);
        }
    }
    
    if (cnt.route!=null) {
        cnt.route.process_dns(cnt);
        
        cnt.response.send();
    } else {
        Light.resolve.DNS({
            name: cnt.fqdn,
            type: 'A',
        }, function (type, err) {
            if (type=='timeout') {
                console.log("\t\t#) Timeout in making request ...");
            } else {
                console.log("\t#) Error on '"+cnt.fqdn+"' : ", err);
            }
        }, function (resp, err) {
            resp.answer.forEach(function (record) {
                console.log("\t\t=> ", record.type, " : ", record.address);
                
                cnt.response.answer.push(record);
            });
        }, function () {
            // console.log('Finished processing request: ' + delta.toString() + 'ms');
            
            cnt.response.send();
        });
    }
};

/**************************************************************************************************************/

exports.initialize = function (Light) {
    exports.routes.push({
        detect: function (context, route) {
            var resp = context.fqdn.match( /(authority|code|collab|health|insights|mail|status)\.(.+)\.zone/i );
            // var resp = context.fqdn.match( /(.+)\.(.+)\.tools/i );
            
            if (resp!=null) {
                context.sparkle = resp[1];
                context.organization = resp[2];
                
                context.route = route;
            }
            
            return context;
        },
        process_dns: function (context) {
            for (i=0 ; i<Light.cfg.tools[context.organization][context.sparkle].targets.length ; i++) {
                context.response.answer.push(dns.CNAME({
                    name: context.sparkle+'.'+context.organization+'.zone',
                    data: Light.cfg.tools[context.organization][context.sparkle].targets[i],
                    ttl: 600,
                }));
            }
        },
    });
    
    exports.server = dns.createServer();
    
    exports.server.on('request', function (req, resp) {
        var cnt = new exports.Context(req, resp);
        
        cnt.process(Light);
    });
    
    exports.server.on('error', function (err, buff, req, res) {
        console.log(err.stack);
    });
};

exports.lunch = function (port) {
    console.log("Started DNS server at 0.0.0.0:"+port);
    
    exports.server.serve(port);
};

