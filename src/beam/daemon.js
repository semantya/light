var sys=require('sys'), util = require('util'), url=require('url'), S=require('string'), YAML = require('yamljs');

/**************************************************************************************************************/

var Light = require('./light');

/**************************************************************************************************************/

var Daemons = {};
var Components = ['dns', 'http'];

for (var i=0 ; i<Components.length ; i++) {
    Daemons[Components[i]] = require('./sparkle-'+Components[i].toLowerCase());
    
    Daemons[Components[i]].server = null;
    Daemons[Components[i]].routes = [];
}

for (var i=0 ; i<Components.length ; i++) {
    Daemons[Components[i]].initialize(Light);
}

for (var i=0 ; i<Components.length ; i++) {
    Daemons[Components[i]].lunch(Light.ports[Components[i].toUpperCase()]);
}

