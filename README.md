# Shell commands #

* dehliz
* shield
* uplink

# Features #

* Linux gateway with firewall / routing capablities in IPv4 / IPv6 & transparent proxying.
* Basic daemons with Caching abalities & Proxying features.

* Configure fileshares across Homeless hosts.
* Configure fileshares across multiple platforms.

* Basic defense daemons with evasive abalities.
* Decentralized anti-DDoS barrier.
* 2-way learning barrier.
* Management & annonce of REST-able resources cache/env with NoSQL backend.

# List of packages #

### Log Analysis : ###

* denyhosts
* fail2ban

### Traffic Analysis : ###

* psad

### File Analysis : ###

* clamav
* chkrootkit
* rkhunter

### Networking : ###

* quagga
* ufw

* isc-dhcp-server

* hostapd
* wvdial

### Tunneling & VPN : ###

* ipsec
* openvpn

### Cache / Proxy : ###

* apt-cacher-ng

### Storage : ###

* vsFTPd
* NFS Server
* Net-A-Talk
* Samba4

